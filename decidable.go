package main

// The abstract behaviour of referee's
type Decidable interface {
	
	Decide(event Event) Decision;
}
