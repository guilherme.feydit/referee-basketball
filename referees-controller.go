package main

type Strategy string

const (
   Honest Strategy = "honest"
   Corrupt = "corrupt"
   EagleEyes = "eagle-eyes"
)

var referees = map[Strategy]Decidable {
	Honest   : HonestReferee{},
	Corrupt  : CorruptReferee{},
	EagleEyes: EagleEyesReferee{},
}

//TODO: Implement the validation function
func IsStrategy() bool {
	return false
}
