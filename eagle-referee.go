package main

type EagleEyesReferee struct {
	Referee
}

func (EagleEyesReferee) Decide(event Event) Decision {
	return Decision{infraction: "Penalty", interpretation: "I Saw It"}
}

func (EagleEyesReferee) String() Strategy {
	return EagleEyes
}
